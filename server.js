const express = require('express');
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const fetch = require('node-fetch');
const bodyParser = require('body-parser');
const app = express();

const auth = require('./controllers/auth');
const log = require('./controllers/log');

const whitelist = ['http://localhost:8080', 'https://wit.masukin.link'];
const corsOptions = {
  origin: function (origin, callback) {
    if (!origin) return callback(null, true);
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

app.use(helmet());
app.use(morgan('combined'));
app.use(cors(corsOptions));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

app.post('/generate-password', (req, res) => {
  auth.generatePassword(req, res, bcrypt);
});

app.post('/signin', (req, res) => {
  auth.handleSignin(req, res, bcrypt);
});

app.get('/check-session', (req, res) => {
  auth.checkSession(req, res);
});

app.post('/logout/:id', (req, res) => {
  auth.handleLogout(req, res);
});

app.get('/dashboard', (req, res) => {
  const data = {
    label: ['00:00', '06:00', '12:00', '18:00', '24:00'],
    value: {
      female: [40, 60, 45, 80, 70],
      male: [30, 50, 55, 75, 60],
    },
  };
  return res.status(200).json({ data });
});

app.use(auth.requireAuth, log.logAction, async (req, res) => {
  try {
    const payload = {
      method: req.method,
    };
    if (req.method !== 'GET') {
      payload.body = JSON.stringify(req.body);
      payload.headers = { 'Content-Type': 'application/json' };
    }
    const resp = await fetch(
      `https://dashboard-wit-api.herokuapp.com${req.url}`,
      payload
    );
    const results = await resp.json();
    return res.status(resp.status).json({ data: results });
  } catch (e) {
    return res
      .status(400)
      .json({ message: 'Terjadi kekeliruan saat mengakses data' });
  }
});

app.listen(process.env.PORT || 3001, () => console.log('Server is running...'));
