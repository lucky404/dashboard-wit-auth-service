const jwt = require('jsonwebtoken');
const util = require('util');
const fetch = require('node-fetch');

const jwtVerifyAsync = util.promisify(jwt.verify);
const JWT_SECRET = 'dashboard_wit';

const signToken = (username, id) => {
  const jwtPayload = { username, id };
  return jwt.sign(jwtPayload, JWT_SECRET, { expiresIn: '2 days' });
};

const generatePassword = (req, res, bcrypt) => {
  const { password } = req.body;
  if (!password) {
    return res.status(400).json({ message: 'Lengkapi Form' });
  }
  const hash = bcrypt.hashSync(password);
  return res.status(201).json({ data: { password: hash } });
};

const handleSignin = async (req, res, bcrypt) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(400).json({ message: 'Lengkapi Form' });
  }
  try {
    const resp = await fetch('https://dashboard-wit-api.herokuapp.com/users');
    const users = await resp.json();
    const user = users.filter(
      (user) => user.username === username && user.status !== 'blocked'
    );
    if (user.length < 1) {
      return res
        .status(400)
        .json({ message: 'Password atau username tidak sesuai' });
    } else {
      const isValid = bcrypt.compareSync(password, user[0].password);
      if (isValid) {
        try {
          const resp = await fetch(
            `https://dashboard-wit-api.herokuapp.com/users/${user[0].id}`,
            {
              method: 'PATCH',
              body: JSON.stringify({
                status: 'online',
                isActive: true,
                last_login: new Date(),
                last_active: new Date(),
                updated_at: new Date(),
              }),
              headers: { 'Content-Type': 'application/json' },
            }
          );
          const validUser = await resp.json();
          const token = signToken(user[0].username, user[0].id);
          return res.json({ data: { ...validUser, token } });
        } catch (e) {
          return res
            .status(400)
            .json({ message: 'Tidak bisa mengakses data user' });
        }
      } else {
        return res
          .status(400)
          .json({ message: 'Password atau username tidak sesuai' });
      }
    }
  } catch (e) {
    return res
      .status(400)
      .json({ message: 'Terjadi kekeliruan saat mengakses data user' });
  }
};

const handleLogout = async (req, res, bcrypt) => {
  const { id } = req.params;
  try {
    const resp = await fetch(
      `https://dashboard-wit-api.herokuapp.com/users/${id}`,
      {
        method: 'PATCH',
        body: JSON.stringify({
          status: 'offline',
          isActive: false,
        }),
        headers: { 'Content-Type': 'application/json' },
      }
    );
    const user = await resp.json();
    return res.status(200).json({ data: user });
  } catch (e) {
    return res
      .status(400)
      .json({ message: 'Terjadi kekeliruan saat mengakses data user' });
  }
};

const getUser = async (id) => {
  try {
    const resp = await fetch(
      `https://dashboard-wit-api.herokuapp.com/users/${id}`
    );
    const user = await resp.json();
    return user;
  } catch (e) {
    throw e;
  }
};

const checkSession = async (req, res) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res
      .status(401)
      .json({ message: 'Tidak terautentikasi', error_code: 'unauthorized' });
  }
  try {
    const tokenInfo = await jwtVerifyAsync(authorization, JWT_SECRET);
    const user = await getUser(tokenInfo.id);
    if (user.status === 'offline' && !user.isActive) {
      return res
        .status(401)
        .json({ message: 'Token Invalid', error_code: 'token_expired' });
    } else {
      return res.status(200).json({ message: 'sesi valid' });
    }
  } catch (err) {
    return res
      .status(401)
      .json({ message: 'Token Invalid', error_code: 'token_expired' });
  }
};

const requireAuth = async (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res
      .status(401)
      .send({ message: 'Tidak Terautentikasi', error_code: 'unauthorized' });
  }
  try {
    const tokenInfo = await jwtVerifyAsync(authorization, JWT_SECRET);
    req.username = tokenInfo.username;
    const user = await getUser(tokenInfo.id);
    if (user.status === 'offline' && !user.isActive) {
      return res
        .status(401)
        .json({ message: 'Token Invalid', error_code: 'token_expired' });
    } else {
      return next();
    }
  } catch (err) {
    return res
      .status(401)
      .json({ message: 'Token Invalid', error_code: 'token_expired' });
  }
};

module.exports = {
  generatePassword,
  handleSignin,
  checkSession,
  requireAuth,
  handleLogout,
};
