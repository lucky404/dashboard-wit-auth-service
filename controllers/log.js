const fetch = require('node-fetch');

const logAction = async (req, res, next) => {
  const { url, method, username } = req;
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  try {
    await fetch(`https://dashboard-wit-api.herokuapp.com/logs`, {
      method: 'POST',
      body: JSON.stringify({
        username,
        ip,
        action: `${url} | ${method}`,
        description: 'Client Access',
        created_at: new Date(),
        updated_at: new Date(),
      }),
      headers: { 'Content-Type': 'application/json' },
    });
    return next();
  } catch (e) {
    return res.status(400).json({ message: 'Terjadi kekeliruan' });
  }
};
module.exports = {
  logAction,
};
